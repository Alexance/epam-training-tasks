﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace IQueryableTask
{
    class YqlQueryExpressionVisitor : ExpressionVisitor
    {
        private readonly StringBuilder builder = new StringBuilder();

        public YqlQueryExpressionVisitor() { }

        Dictionary<string, string> replaces = new Dictionary<string, string>() {
            {"Type", "type"},
            {"Category", "category_name"}
        };

        /// <summary>
        /// Get string representation from <value>expression</value>
        /// </summary>
        /// <param name="expression">Expression to convert</param>
        /// <returns>String which represents <value>expression</value> as text</returns>
        public string GetStringRepresentation(Expression expression)
        {
            Visit(expression);

            string result = builder.ToString().ToLower();

            if (result.IndexOf("ChosenAnswer", StringComparison.InvariantCultureIgnoreCase) > -1 || result.IndexOf("Content", StringComparison.InvariantCultureIgnoreCase) > -1)
                throw new NotSupportedException();

            // 'Query' field not exist
            if (result.IndexOf("query") == -1)
                throw new InvalidOperationException();

            return result;
        }

        private Expression StripQuotes(Expression expression)
        {
            while (expression.NodeType == ExpressionType.Quote)
            {
                expression = ((UnaryExpression)expression).Operand;
            }

            return expression;
        }

        protected override Expression VisitMethodCall(MethodCallExpression methodCallExpression)
        {
            if (methodCallExpression.Method.Name == "Where")
            {

                builder.Append("SELECT * FROM answers.search");

                this.Visit(methodCallExpression.Arguments[0]);
                builder.Append(" WHERE ");

                LambdaExpression lambda = (LambdaExpression)(StripQuotes(methodCallExpression.Arguments[1]));
                Visit(lambda.Body);

                return methodCallExpression;
            }
            else if (methodCallExpression.Method.Name == "Contains")
            {
                builder.Append("query=");
                Visit(methodCallExpression.Arguments[0]);

                return methodCallExpression;
            }
            else
                throw new InvalidOperationException();
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            // Select part for visit. If one is ExpressionType.Convert, it will be first for convert
            if (node.Right.NodeType == ExpressionType.Convert)
                this.Visit(node.Right);
            else
                this.Visit(node.Left);

            // Visit operator between expressions
            switch (node.NodeType)
            {
                case ExpressionType.And:
                case ExpressionType.AndAlso:
                    builder.Append(" AND ");
                    break;
                case ExpressionType.Or:
                case ExpressionType.OrElse:
                    builder.Append(" OR ");
                    break;
                case ExpressionType.Equal:
                    builder.Append("=");
                    break;
                default:
                    throw new NotSupportedException(string.Format("The binary operator '{0}' is not supported", node.NodeType));
            }

            // Part for convert was converted ahead.
            // Select another part for visiting
            if (node.Right.NodeType == ExpressionType.Convert)
                this.Visit(node.Left);
            else
                this.Visit(node.Right);

            return node;
        }

        protected override Expression VisitUnary(UnaryExpression node)
        {
            switch (node.NodeType)
            {
                case ExpressionType.Not:
                    builder.Append(" NOT ");
                    this.Visit(node.Operand);
                    break;
                case ExpressionType.Convert:
                    this.Visit(node.Operand);
                    break;
                default:
                    return node;
            }
            return node;
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            if (node.Value == null)
            {
                builder.Append("NULL");
            }
            else if (typeToConvert != null && typeToConvert.IsEnum)
            {
                builder.Append('\"');
                builder.Append(Enum.GetName(typeToConvert, node.Value));
                builder.Append('\"');
                typeToConvert = null;
            }
            else
            {
                switch (Type.GetTypeCode(node.Value.GetType()))
                {
                    case TypeCode.String:
                    case TypeCode.Int32:
                        builder.Append('\"');
                        builder.Append(node.Value);
                        builder.Append('\"');
                        break;
                    default:
                        return node;
                }
            }

            return node;
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            if (node.Name != null && node.NodeType == ExpressionType.Parameter)
                builder.Append(node.Name);

            return node;
        }

        private Type typeToConvert { get; set; }

        protected override Expression VisitMember(MemberExpression node)
        {
            if (node.Expression != null && node.Expression.NodeType == ExpressionType.Parameter)
            {
                // We can add custom key to dictionary for replace
                string replace = replaces.ContainsKey(node.Member.Name) ? replaces[node.Member.Name] : node.Member.Name;

                builder.Append(replace);
                typeToConvert = node.Type;
                return node;
            }

            throw new NotSupportedException();
        }
    }
}
