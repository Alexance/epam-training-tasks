﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace IQueryableTask
{
    public class YqlAnswersQueryProvider : IQueryProvider
    {
        public IQueryable CreateQuery(Expression expression)
        {
            // TODO: Implement CreateQuery
            // throw new NotImplementedException();
            Type elementType = expression.Type;
            try {
                return (IQueryable) Activator.CreateInstance(typeof(YqlAnswerSearch), new object[] { expression });
            }
            catch (System.Reflection.TargetInvocationException tie)
            {
                throw tie.InnerException;
            }
        }

        public IQueryable<Question> CreateQuery<Question>(Expression expression)
        {
            // TODO: Implement CreateQuery
            // throw new NotImplementedException();
            return (IQueryable<Question>)new YqlAnswerSearch(expression);
        }

        public object Execute(Expression expression)
        {
            // TODO: Implement Execute
            //throw new NotImplementedException();

            // HINT: Use GetYqlQuery to build query
            // HINT: Use YqlAnswersService to fetch data
            string query = this.GetYqlQuery(expression);
            YqlAnswersService queryService = new YqlAnswersService();

            return queryService.Search(query);
        }

        public TResult Execute<TResult>(Expression expression)
        {
            // TODO: Implement Execute
            // throw new NotImplementedException();

            return (TResult) Execute(expression);
        }

        /// <summary>
        /// Generates YQL Query
        /// </summary>
        /// <param name="expression">Expression tree</param>
        /// <returns></returns>
        public string GetYqlQuery(Expression expression)
        {
            // TODO: Implement GetYqlQuery
            // throw new NotImplementedException();

            // HINT: Create a class derived from ExpressionVisitor
            var visitor = new YqlQueryExpressionVisitor();

            return visitor.GetStringRepresentation(expression);
        }
    }
}
