﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace LinqToXml
{
    // TODO : Все таже проблема с ToList и ToArray практически везде, не буду каждый раз указывать 
    
    public static class LinqToXml
    {
        /// <summary>
        /// Creates hierarchical data grouped by category
        /// </summary>
        /// <param name="xmlRepresentation">Xml representation (refer to CreateHierarchySourceFile.xml in Resources)</param>
        /// <returns>Xml representation (refer to CreateHierarchyResultFile.xml in Resources)</returns>
        public static string CreateHierarchy(string xmlRepresentation)
        {
            // throw new NotImplementedException();
            XDocument xmlDoc = XDocument.Parse(xmlRepresentation);
            XDocument newXml = new XDocument(new XElement("Root"));

            // TODO : Тут очень много кода написано, что-то добавляется, потом что-то удаляется. Понять что делает метод - очень сложно
            // Рекомендую использовать functional constructor : https://msdn.microsoft.com/en-us/library/vstudio/bb387021(v=vs.100).aspx

            // Fixed

            var allDataElements = xmlDoc.Element("Root").Descendants("Data");

            // Get all category names
            var categories = xmlDoc.Element("Root").Descendants("Category").Select(x => x.Value).Distinct();

            foreach (var category in categories)
            {
                // Combined data with 'Category' field
                var group = new XElement("Group", new XAttribute("ID", category), allDataElements.Where(x => x.Element("Category").Value == category));

                group.Descendants("Category").Remove();

                newXml.Element("Root").Add(group);
            }

            return newXml.ToString();
        }

        /// <summary>
        /// Get list of orders numbers (where shipping state is NY) from xml representation
        /// </summary>
        /// <param name="xmlRepresentation">Orders xml representation (refer to PurchaseOrdersSourceFile.xml in Resources)</param>
        /// <returns>Concatenated orders numbers</returns>
        /// <example>
        /// 99301,99189,99110
        /// </example>
        public static string GetPurchaseOrders(string xmlRepresentation)
        {
            // throw new NotImplementedException();
            XDocument xmlDoc = XDocument.Parse(xmlRepresentation);
            XNamespace aw = "http://www.adventure-works.com";

            var queryResult = xmlDoc.Element(aw + "PurchaseOrders").Elements(aw + "PurchaseOrder")
                .Where(x => x.Elements(aw + "Address")
                    .Any(y => y.Attribute(aw + "Type").Value == "Shipping" && y.Element(aw + "State").Value == "NY"))
                .Select(x => x.Attribute(aw + "PurchaseOrderNumber").Value);

            return string.Join(",", queryResult);
        }

        /// <summary>
        /// Reads csv representation and creates appropriate xml representation
        /// </summary>
        /// <param name="customers">Csv customers representation (refer to XmlFromCsvSourceFile.csv in Resources)</param>
        /// <returns>Xml customers representation (refer to XmlFromCsvResultFile.xml in Resources)</returns>
        public static string ReadCustomersFromCsv(string customers)
        {
            //throw new NotImplementedException();
            const int col_CustomerId = 0, col_CompanyName = 1, col_ContactName = 2, col_ContactTitle = 3, col_Phone = 4,
                col_Address = 5, col_City = 6, col_Region = 7, col_PostalCode = 8, col_Country = 9;

            string[] values = customers.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            XDocument document = new XDocument();
            document.Add(new XElement("Root"));

            foreach (var value in values)
            {
                var parsedCsv = value.Split(new char[] { ',' });   

                document.Element("Root").Add(
                    new XElement("Customer",
                        new XAttribute("CustomerID", parsedCsv[col_CustomerId]),    // TODO : Магические цифры, args[0], ..., args[9] - что они значат?
                        new XElement("CompanyName", parsedCsv[col_CompanyName]),     // Принято заводить константы и обращаться так : args[col_CustomerId] , ..., args[col_Country],
                        new XElement("ContactName", parsedCsv[col_ContactName]),     // тогда становиться все понятно и намного легче модифицировать код в будущем
                        new XElement("ContactTitle", parsedCsv[col_ContactTitle]),
                        new XElement("Phone", parsedCsv[col_Phone]),
                        new XElement("FullAddress",
                            new XElement("Address", parsedCsv[col_Address]),
                            new XElement("City", parsedCsv[col_City]),
                            new XElement("Region", parsedCsv[col_Region]),
                            new XElement("PostalCode", parsedCsv[col_PostalCode]),
                            new XElement("Country", parsedCsv[col_Country])))
                );
            }

            return document.ToString();
        }

        /// <summary>
        /// Gets recursive concatenation of elements
        /// </summary>
        /// <param name="xmlRepresentation">Xml representation of document with Sentence, Word and Punctuation elements. (refer to ConcatenationStringSource.xml in Resources)</param>
        /// <returns>Concatenation of all this element values.</returns>
        public static string GetConcatenationString(string xmlRepresentation)
        {
            // throw new NotImplementedException();
            // Fixed
            XDocument xmlDoc = XDocument.Parse(xmlRepresentation);

            return string.Join(null, xmlDoc.Elements().Select(x => x.Value));
        }

        // TODO : Изобретение велосипеда. RTFM Descendants и DescendantNodes. В общем, создание списка и сохранение туда промежуточных данных - это ToLIst\ToArray стиль.
        // LINQ был изобретен для работы с данными на лету через yeild return, без хранения в памяти миллионов промежуточных элементов
        // Deleted

        /// <summary>
        /// Replaces all "customer" elements with "contact" elements with the same childs
        /// </summary>
        /// <param name="xmlRepresentation">Xml representation with customers (refer to ReplaceCustomersWithContactsSource.xml in Resources)</param>
        /// <returns>Xml representation with contacts (refer to ReplaceCustomersWithContactsResult.xml in Resources)</returns>
        public static string ReplaceAllCustomersWithContacts(string xmlRepresentation)
        {
            // throw new NotImplementedException();
            XDocument xmlDoc = XDocument.Parse(xmlRepresentation);
            XElement xElem = new XElement("Document", xmlDoc.Element("Document").Elements().Select(x => new XElement("contact", x.Elements())));

            return xElem.ToString();
        }

        /// <summary>
        /// Finds all ids for channels with 2 or more subscribers and mark the "DELETE" comment
        /// </summary>
        /// <param name="xmlRepresentation">Xml representation with channels (refer to FindAllChannelsIdsSource.xml in Resources)</param>
        /// <returns>Sequence of channels ids</returns>
        public static IEnumerable<int> FindChannelsIds(string xmlRepresentation)
        {
            // throw new NotImplementedException();
            XDocument xmlDoc = XDocument.Parse(xmlRepresentation);

            return xmlDoc.Element("service").Elements()
                .Where(x => x.Elements().Count() >= 2 && x.DescendantNodes().Any(y => y.NodeType == System.Xml.XmlNodeType.Comment && ((XComment) y).Value  == "DELETE"))
                .Select(x => int.Parse((string) x.Attribute("id")));
        }

        /// <summary>
        /// Sort customers in docement by Country and City
        /// </summary>
        /// <param name="xmlRepresentation">Customers xml representation (refer to GeneralCustomersSourceFile.xml in Resources)</param>
        /// <returns>Sorted customers representation (refer to GeneralCustomersResultFile.xml in Resources)</returns>
        public static string SortCustomers(string xmlRepresentation)
        {
            // throw new NotImplementedException();
            XDocument xmlDocument = XDocument.Parse(xmlRepresentation);
            var sortedElements = xmlDocument.Element("Root").Elements()
                .OrderBy(x => x.Element("FullAddress").Element("Country").Value)
                .ThenBy(x => x.Element("FullAddress").Element("City").Value);


            return (new XElement("Root", sortedElements.ToArray())).ToString();
        }

        /// <summary>
        /// Gets XElement flatten string representation to save memory
        /// </summary>
        /// <param name="xmlRepresentation">XElement object</param>
        /// <returns>Flatten string representation</returns>
        /// <example>
        ///     <root><element>something</element></root>
        /// </example>
        public static string GetFlattenString(XElement xmlRepresentation)
        {
            // TODO : ?????
            // I haven't tests and resources for this method!

            // throw new NotImplementedException();
            return xmlRepresentation.ToString();
        }

        /// <summary>
        /// Gets total value of orders by calculating products value
        /// </summary>
        /// <param name="xmlRepresentation">Orders and products xml representation (refer to GeneralOrdersFileSource.xml in Resources)</param>
        /// <returns>Total purchase value</returns>
        public static int GetOrdersValue(string xmlRepresentation)
        {
            // throw new NotImplementedException();
            XDocument xmlDocument = XDocument.Parse(xmlRepresentation);
            
            // TODO : Тут типичная задача на Join
            // Fixed
            var orders = xmlDocument.Element("Root").Element("Orders").Elements("Order");
            var productsCosts = xmlDocument.Element("Root").Element("products").Elements();

            return orders.Join(productsCosts, x => x.Element("product").Value, y => y.Attribute("Id").Value, (x, y) => int.Parse(y.Attribute("Value").Value)).Sum();
        }
    }
}
