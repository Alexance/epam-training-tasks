﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.IO.Packaging;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Linq;

namespace IOStreams
{

	public static class TestTasks
	{
		/// <summary>
		/// Parses Resourses\Planets.xlsx file and returns the planet data: 
		///   Jupiter     69911.00
		///   Saturn      58232.00
		///   Uranus      25362.00
		///    ...
		/// See Resourses\Planets.xlsx for details
		/// </summary>
		/// <param name="xlsxFileName">source file name</param>
		/// <returns>sequence of PlanetInfo</returns>
		public static IEnumerable<PlanetInfo> ReadPlanetInfoFromXlsx(string xlsxFileName)
		{
			// TODO : Implement ReadPlanetInfoFromXlsx method using System.IO.Packaging + Linq-2-Xml

			// HINT : Please be as simple & clear as possible.
			//        No complex and common use cases, just this specified file.
			//        Required data are stored in Planets.xlsx archive in 2 files:
			//         /xl/sharedStrings.xml      - dictionary of all string values
			//         /xl/worksheets/sheet1.xml  - main worksheet

			// throw new NotImplementedException();

            IEnumerable<double> values;
            IEnumerable<string> names;

            using (var fileStream = File.OpenRead(xlsxFileName))
            {
                ZipArchive archive = new ZipArchive(fileStream);
                ZipArchiveEntry sharedStrings = archive.GetEntry("xl/sharedStrings.xml");
                ZipArchiveEntry worksheet = archive.GetEntry("xl/worksheets/sheet1.xml");

                XNamespace xmlns = "http://schemas.openxmlformats.org/spreadsheetml/2006/main";
                using (var sharedStringValues = sharedStrings.Open())
                {
                    XDocument doc = XDocument.Load(sharedStringValues);
                    names = doc.Root
                        .Elements(xmlns + "si")
                        .Select(x => x.Element(xmlns + "t").Value)
                        .Take(int.Parse((string)doc.Root.Attribute("count"), System.Globalization.CultureInfo.InvariantCulture) - 2)
                        .ToList();
                }

                using (var worksheetValues = worksheet.Open())
                {
                    XDocument doc = XDocument.Load(worksheetValues);

                    values = doc.Root.Element(xmlns + "sheetData")
                        .Elements(xmlns + "row")
                        .Skip(1)
                        .SelectMany(x => x.Elements(xmlns + "c"))
                        .Where(y => ((string)y.Attribute("r")).IndexOf('B') != -1)
                        .Select(x => double.Parse(x.Element(xmlns + "v").Value, System.Globalization.CultureInfo.InvariantCulture))
                        .ToList();
                }
            }

            return names.Zip(values, (x, y) => new PlanetInfo() { Name = x, MeanRadius = y }).ToList();
		}


		/// <summary>
		/// Calculates hash of stream using specifued algorithm
		/// </summary>
		/// <param name="stream">source stream</param>
		/// <param name="hashAlgorithmName">hash algorithm ("MD5","SHA1","SHA256" and other supported by .NET)</param>
		/// <returns></returns>
		public static string CalculateHash(this Stream stream, string hashAlgorithmName)
		{
			// TODO : Implement CalculateHash method
			// throw new NotImplementedException();
            HashAlgorithm algorithm;
            try
            {
                algorithm = HashAlgorithm.Create(hashAlgorithmName);
                algorithm.Initialize();
            }
            catch
            {
                throw new ArgumentException();
            }

            int readedBytes = 0;
            byte[] readed = new byte[128];

            while ((readedBytes = stream.Read(readed, 0, readed.Length)) > 0)
            {
                algorithm.TransformBlock(readed, 0, readedBytes, null, 0);
            }
            algorithm.TransformFinalBlock(new byte[0], 0, 0);

            return GetStringFromHashBytes(algorithm.Hash);
		}

        private static string GetStringFromHashBytes(byte[] hash)
        {
            StringBuilder sb = new StringBuilder(hash.Length);
            for (int i = 0; i < hash.Length; i++)
                sb.Append(hash[i].ToString("x2"));

            return sb.ToString().ToUpper();
        }


		/// <summary>
		/// Returns decompressed strem from file. 
		/// </summary>
		/// <param name="fileName">source file</param>
		/// <param name="method">method used for compression (none, deflate, gzip)</param>
		/// <returns>output stream</returns>
		public static Stream DecompressStream(string fileName, DecompressionMethods method)
		{
			// TODO : Implement DecompressStream method
			//throw new NotImplementedException();

            MemoryStream ms = new MemoryStream();
            byte[] buffer = new byte[1024];
            int readedBytes;

            using (FileStream stream = File.OpenRead(fileName))
            {
                while ((readedBytes = stream.Read(buffer, 0, buffer.Length)) > 0)
                    ms.Write(buffer, 0, readedBytes);
            }

            ms.Flush();
            ms.Seek(0, SeekOrigin.Begin);

            switch (method)
            {
                case DecompressionMethods.Deflate:
                    return new DeflateStream(ms, CompressionMode.Decompress);
                case DecompressionMethods.GZip:
                    return new GZipStream(ms, CompressionMode.Decompress);
                default:
                    return ms;
            }
		}


		/// <summary>
		/// Reads file content econded with non Unicode encoding
		/// </summary>
		/// <param name="fileName">source file name</param>
		/// <param name="encoding">encoding name</param>
		/// <returns>Unicoded file content</returns>
		public static string ReadEncodedText(string fileName, string encoding)
		{
			// TODO : Implement ReadEncodedText method
			// throw new NotImplementedException();
            
            //byte[] buffer = new byte[1024]; // Readed bytes from stream
            //int readedBytes;
            //StringBuilder sb = new StringBuilder();
            //Encoding converter = Encoding.GetEncoding(encoding);

            //using (FileStream stream = File.OpenRead(fileName))
            //{
            //    while ((readedBytes = stream.Read(buffer, 0, buffer.Length)) > 0)
            //        sb.Append(converter.GetString(buffer, 0, readedBytes));
            //}

            //return sb.ToString();

            /// More simple method

            return File.ReadAllText(fileName, Encoding.GetEncoding(encoding));
		}
	}


	public class PlanetInfo : IEquatable<PlanetInfo>
	{
		public string Name { get; set; }
		public double MeanRadius { get; set; }

		public override string ToString()
		{
			return string.Format("{0} {1}", Name, MeanRadius);
		}

		public bool Equals(PlanetInfo other)
		{
			return Name.Equals(other.Name)
				&& MeanRadius.Equals(other.MeanRadius);
		}
	}



}
