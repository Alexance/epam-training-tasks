﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Reflection;
using System.Linq;


namespace Serialization.Tasks
{
    // TODO : Make Company class xml-serializable using DataContractSerializer 
    // Employee.Manager should be serialized as reference
    // Company class has to be forward compatible with all derived versions


    [DataContract]
    [Serializable()]
    [KnownType("GetKnownTypes")]
    public class Company : IExtensibleDataObject
    {
        private ExtensionDataObject extensionDataObject_value;
        public ExtensionDataObject ExtensionData
        {
            get
            {
                return extensionDataObject_value;
            }
            set
            {
                extensionDataObject_value = value;
            }
        }

        [DataMember()]
        public string Name { get; set; }
        [DataMember()]
        public IList<Employee> Employee { get; set; }

        // More universal method with comparing of [KnownType(Type type)] attribute using, but slow :(

        public static Type[] GetKnownTypes()
        {
            Type thisType = MethodBase.GetCurrentMethod().DeclaringType;
            return thisType.Assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(Employee))).ToArray();
        }
    }

    [DataContract(IsReference = true)]
    public abstract class Employee {
        [DataMember()]
        public string Name { get; set; }
        [DataMember()]
        public string LastName { get; set; }
        [DataMember()]
        public string Title { get; set; }
        [DataMember()]
        public Manager Manager { get; set; }
    }

    [DataContract()]
    public class Worker : Employee {
        [DataMember()]
        public int Salary { get; set; }
    }

    [DataContract(IsReference = true)]
    public class Manager : Employee {
        [DataMember()]
        public int YearBonusRate { get; set; } 
    }

}
