﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncIO
{
    public static class Tasks
    {
        /// <summary>
        /// Returns the content of required uris.
        /// Method has to use the synchronous way and can be used to compare the performace of sync \ async approaches. 
        /// </summary>
        /// <param name="uris">Sequence of required uri</param>
        /// <returns>The sequence of downloaded url content</returns>
        public static IEnumerable<string> GetUrlContent(this IEnumerable<Uri> uris) 
        {
            // TODO : Implement GetUrlContent
            // throw new NotImplementedException();
            foreach (Uri uri in uris)
            {
                yield return LoadData(uri);
            }
        }

        private static string LoadData(Uri uri)
        {
            try
            {
                using (WebClient client = new WebClient())
                {
                    return client.DownloadString(uri);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.InnerException.Message);

                throw ex;
            }
        }


        /// <summary>
        /// Returns the content of required uris.
        /// Method has to use the asynchronous way and can be used to compare the performace of sync \ async approaches. 
        /// 
        /// maxConcurrentStreams parameter should control the maximum of concurrent streams that are running at the same time (throttling). 
        /// </summary>
        /// <param name="uris">Sequence of required uri</param>
        /// <param name="maxConcurrentStreams">Max count of concurrent request streams</param>
        /// <returns>The sequence of downloaded url content</returns>
        public static IEnumerable<string> GetUrlContentAsync(this IEnumerable<Uri> uris, int maxConcurrentStreams)
        {
            // TODO : Implement GetUrlContentAsync
            // throw new NotImplementedException();

            Uri[] urisArray = uris.ToArray();
            int minimal = Math.Min(urisArray.Length, maxConcurrentStreams);

            Task<string>[] tasks = new Task<string>[minimal];

            HttpClient client = new HttpClient();

            for (int i = 0; i < minimal; i++)
            {
                tasks[i] = client.GetStringAsync(urisArray[i]);
            }

            for (int i = maxConcurrentStreams; i < minimal; i++)
            {
                // Wait for first completed task
                int completed = Task.WaitAny(tasks);
                yield return tasks[completed].Result;

                // Start new task as replace for completed
                tasks[completed] = client.GetStringAsync(urisArray[i]);
            }

            Task.WaitAll(tasks);
            for (int i = 0; i < tasks.Length; i++)
            {
                if (tasks[i] != null)
                    yield return tasks[i].Result;
            }

            //List<string> results = new List<string>();
            //int totalInProgress = 0;


            //client.DownloadStringCompleted += (sender, e) =>
            //{
            //    results.Add(e.Result);
            //    totalInProgress--;
            //};

            //for (int i = 0; i < urisArray.Length; i++)
            //{
            //    if (totalInProgress < maxConcurrentStreams)
            //    {


            //        totalInProgress++;
            //        client.DownloadStringAsync(urisArray[i]);
            //    }
            //    else
            //    {
            //        i--;
            //    }                
            //}

            //return results;
        }

        /// <summary>
        /// Calculates MD5 hash of required resource.
        /// 
        /// Method has to run asynchronous. 
        /// Resource can be any of type: http page, ftp file or local file.
        /// </summary>
        /// <param name="resource">Uri of resource</param>
        /// <returns>MD5 hash</returns>
        public async static Task<string> GetMD5Async(this Uri resource)
        {
            // TODO : Implement GetMD5Async
            // throw new NotImplementedException();
            using (WebClient client = new WebClient())
            {
                MD5 md5Hasher = MD5.Create();
                byte[] downloaded = await client.DownloadDataTaskAsync(resource);
                return string.Concat(md5Hasher.ComputeHash(downloaded).Select(x => x.ToString("x2")));
            }
        }

    }



}
